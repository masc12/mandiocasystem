package com.academico.commd.repository;

import com.academico.commd.model.Desempenho;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DesempenhoRepository extends JpaRepository<Desempenho, Long> {
}
