package com.academico.commd.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.academico.commd.model.Curso;

@Repository
public interface CursoRepository extends JpaRepository<Curso, Long> {
	
	
	public List<Curso> findCursoByNome(String nome);
	public List<Curso> findCursoByTipo(String tipo);
}
