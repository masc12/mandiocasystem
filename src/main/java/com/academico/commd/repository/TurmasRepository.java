package com.academico.commd.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.academico.commd.model.Turma;

public interface TurmasRepository extends JpaRepository<Turma, Long> {

	public List<Turma> findTurmaBySemestre(double semestre);
	public List<Turma> findTurmaByAno(Date ano);
}
