package com.academico.commd.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.academico.commd.model.Disciplina;

@Repository
public interface DisciplinaRepository extends JpaRepository<Disciplina, Long> {
	
	public List<Disciplina> findDisciplinaById(long id);
	public List<Disciplina> findDisciplinaByNome(String nome);
	public List<Disciplina> findDisciplinaByArea(String area);
}
