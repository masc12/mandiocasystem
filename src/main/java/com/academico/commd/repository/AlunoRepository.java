package com.academico.commd.repository;

import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.academico.commd.model.Aluno;

@Repository
public interface AlunoRepository extends JpaRepository<Aluno, Long>{
	 
	
	public List<Aluno> findAlunoByNome(String nome);
	public List<Aluno> findAlunoByLogin(String login);
	public List<Aluno> findAlunoByDataVinculo(Date dataVinculo);
	
}
