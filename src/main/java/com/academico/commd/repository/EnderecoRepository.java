package com.academico.commd.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.academico.commd.model.Endereco;

public interface EnderecoRepository extends JpaRepository<Endereco, Long> {
	
	public List<Endereco> findEnderecoByRua(String rua);
	public List<Endereco> findEnderecoByCep(int cep);
	public List<Endereco> findEnderecoByCidade(String cidade);
	public List<Endereco> findEnderecoByEstado(String estado);
}
