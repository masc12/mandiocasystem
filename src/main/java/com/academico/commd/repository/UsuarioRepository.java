package com.academico.commd.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.academico.commd.model.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Long> {
	
	public List<Usuario> findUsuarioByNome(String nome);
	public List<Usuario> findUsuarioByLogin(String login);
}
