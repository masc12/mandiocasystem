package com.academico.commd.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.academico.commd.model.Professor;

public interface ProfessorRepository extends JpaRepository<Professor, Long> {
	
	public List<Professor> findProfessorByNome(String nome);
	public List<Professor> findProfessorByLogin(String login);
	public List<Professor> findProfessorByAreaAtuacao(String areaAtuacao);
	public List<Professor> findProfessorByTitulacao(String titulacao);
}
