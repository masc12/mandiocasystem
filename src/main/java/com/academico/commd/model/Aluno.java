package com.academico.commd.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
public class Aluno extends Usuario {
	
	@ManyToOne
	@JoinColumn(name="curso_id")
	private Curso curso;
	
	@OneToMany(mappedBy="aluno")
	private List<Desempenho> desempenho;
	
//	@NotNull
	@Column
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataVinculo;

	public Date getDataVinculo() {
		return dataVinculo;
	}

	public void setDataVinculo(Date dataVinculo) {
		this.dataVinculo = dataVinculo;
	}

	public Curso getCurso() {
		return curso;
	}
	
	public List<Desempenho> getDesempenho(){
		return desempenho;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((dataVinculo == null) ? 0 : dataVinculo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Aluno other = (Aluno) obj;
		if (dataVinculo == null) {
			if (other.dataVinculo != null)
				return false;
		} else if (!dataVinculo.equals(other.dataVinculo))
			return false;
		return true;
	}
	
	

}
