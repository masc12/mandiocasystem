package com.academico.commd.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotEmpty;

@Entity
public class Turma {
	
	@ManyToOne
	@JoinColumn(name="disciplina_id")
	public Disciplina disciplina;
	
	@ManyToOne
	@JoinColumn(name="professor_id")
	public Professor professor;
	
	@OneToMany(mappedBy="turma")
	public List<Desempenho> desempenho;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
//	@NotEmpty
	@Column
	private double semestre;
	
//	@NotEmpty
	@Temporal(TemporalType.TIMESTAMP)
	private Date ano;
	
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public double getSemestre() {
		return semestre;
	}

	public void setSemestre(double semestre) {
		this.semestre = semestre;
	}

	public Date getAno() {
		return ano;
	}

	public void setAno(Date ano) {
		this.ano = ano;
	}
	
	public Disciplina getDisciplina() {
		return disciplina;
	}
	
	public Professor getProfessor() {
		return professor;
	}
	
	public List<Desempenho> getDesempenho(){
		return desempenho;
	}
}
