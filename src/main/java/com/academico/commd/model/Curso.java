package com.academico.commd.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotEmpty;

@Entity
public class Curso {
	
	@OneToMany(mappedBy = "curso")
	private List<Aluno> aluno;
	
	@ManyToMany
	@JoinTable(name= "curso_professor",
			joinColumns=@JoinColumn(name="curso_id"),
			inverseJoinColumns=@JoinColumn(name="professor_id"))
	private List<Professor> professor;
	
	@OneToMany(mappedBy = "curso")
	private List<Disciplina> disciplina;
	
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@NotEmpty
	@Column(length = 120)
	private String nome;
	
	@NotEmpty
	@Column(length = 20 )
	private String tipo;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	public List<Aluno> getAluno(){
		return aluno;
	}
	
	public List<Professor> getProfessor(){
		return professor;
	}
	
	public List<Disciplina> getDisciplina(){
		return disciplina;
	}
}
