package com.academico.commd.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Desempenho {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@ManyToOne
	@JoinColumn(name="aluno_id")
	private Aluno aluno;
	
	@ManyToOne
	@JoinColumn(name="turma_id")
	private Turma turma;
	
	@OneToMany(mappedBy="desempenho")
	private List<Nota> nota;
	
	private double media;
	
	
	public double getMedia() {
		return media;
	}

	public void setMedia(double media) {
		this.media = media;
	}

	public Aluno getAluno() {
		return aluno;
	}
	
	public Turma getTurma() {
		return turma;
	}
	
	public List<Nota> getNota(){
		return nota;
	}
}
