package com.academico.commd.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotEmpty;

@Entity
public class Disciplina {
	
	@ManyToOne
	@JoinColumn(name="curso_id")
	private Curso curso;
	
	@OneToMany(mappedBy="disciplina")
	private List<Turma> turma;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@NotEmpty
	@Column(length = 120)
	private String nome;
	
	@NotEmpty
	@Column(length = 50)
	private String area;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}
	
	public Curso getCurso() {
		return curso;
	}
	
	public List<Turma> getTurma(){
		return turma;
	}
}
