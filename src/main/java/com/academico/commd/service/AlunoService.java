package com.academico.commd.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.academico.commd.model.Aluno;
import com.academico.commd.repository.AlunoRepository;

@Service
public class AlunoService implements CrudInterface<Aluno> {

	@Autowired
	private AlunoRepository alunoRepository;
	
	public List<Aluno> findByNome(String nome){
		return alunoRepository.findAlunoByNome(nome);
	}
	
	public List<Aluno> findByLogin(String login){
		return alunoRepository.findAlunoByLogin(login);
	}
	
	public List<Aluno> findByDataVinculo(Date dataVinculo){
		return alunoRepository.findAlunoByDataVinculo(dataVinculo);
	}
	
	
	@Override
	public List<Aluno> findAll() {
		// TODO Auto-generated method stub
		return alunoRepository.findAll();
	}

	@Override
	public Optional<Aluno> save(Aluno entity) {
		List<Aluno> alunoExist = alunoRepository.
			findAlunoByNome(entity.getNome());
		if(!alunoExist.isEmpty()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
					"O Aluno com este nome já existe em nossa base de dados!");
		}
		return Optional.of(alunoRepository.save(entity));
	}

	@Override
	public Optional<Aluno> findById(long id) {
		return alunoRepository.findById(id);
	}

	@Override
	public void delete(Aluno entity) {
		// TODO Auto-generated method stub
		alunoRepository.delete(entity);
	}

	@Override
	public void deleteById(long id) {
		// TODO Auto-generated method stub
		alunoRepository.deleteById(id);
	}

	@Override
	public long count() {
		// TODO Auto-generated method stub
		return alunoRepository.count();
	}
	

}
