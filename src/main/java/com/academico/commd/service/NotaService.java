package com.academico.commd.service;

import com.academico.commd.model.Nota;
import com.academico.commd.repository.NotaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class NotaService implements CrudInterface<Nota> {

    @Autowired
    private NotaRepository notaRep;

    @Override
    public List<Nota> findAll() {
        return notaRep.findAll();
    }

    @Override
    public Optional<Nota> save(Nota entity) {
        return Optional.of(notaRep.save(entity));
    }

    @Override
    public Optional<Nota> findById(long id) {
        return notaRep.findById(id);
    }

    @Override
    public void delete(Nota entity) {
        notaRep.delete(entity);
    }

    @Override
    public void deleteById(long id) {
        notaRep.deleteById(id);
    }

    @Override
    public long count() {
        return notaRep.count();
    }
}
