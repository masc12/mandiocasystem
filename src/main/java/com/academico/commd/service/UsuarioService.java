package com.academico.commd.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.academico.commd.model.Usuario;
import com.academico.commd.repository.UsuarioRepository;

@Service
public class UsuarioService implements CrudInterface<Usuario> {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Override
    public List<Usuario> findAll() {
        return usuarioRepository.findAll();
    }
    public List<Usuario> findByLogin(String login){
        return usuarioRepository.findUsuarioByLogin(login);
    }

    public List<Usuario> findByNome(String nome){
        return usuarioRepository.findUsuarioByNome(nome);
    }

    @Override
    public Optional<Usuario> save(Usuario entity) {
        List<Usuario> usuarioNome = usuarioRepository.
                findUsuarioByNome(entity.getNome());
        List<Usuario> usuarioLogin = usuarioRepository.
                findUsuarioByLogin(entity.getLogin());

        if(!usuarioNome.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "Já existe um Usuario com este nome cadastrado "
                            + "em nossa base.");
        } else if(!usuarioLogin.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "Login já esta sendo utilizado por outro usuario.");
        }

        return Optional.of(usuarioRepository.save(entity));
    }

    @Override
    public Optional<Usuario> findById(long id) {
        return usuarioRepository.findById(id);
    }

    @Override
    public void delete(Usuario entity) {
        usuarioRepository.delete(entity);
    }

    @Override
    public void deleteById(long id) {
        usuarioRepository.deleteById(id);
    }

    @Override
    public long count() {
        return usuarioRepository.count();
    }

}
