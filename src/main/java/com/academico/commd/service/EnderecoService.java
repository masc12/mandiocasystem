package com.academico.commd.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import com.academico.commd.model.Endereco;
import com.academico.commd.repository.EnderecoRepository;
import org.springframework.stereotype.Service;

@Service
public class EnderecoService implements CrudInterface<Endereco> {

	@Autowired
	private EnderecoRepository endRep;
	
	@Override
	public List<Endereco> findAll() {
		return endRep.findAll(); 
	}
	
	public List<Endereco> findByRua(String rua){
		return endRep.findEnderecoByRua(rua);
	}
	
	public List<Endereco> findByCep(int cep){
		return endRep.findEnderecoByCep(cep);
	}
	
	public List<Endereco> findByCidade(String cidade){
		return endRep.findEnderecoByCidade(cidade);
	}
	
	public List<Endereco> findByEstado(String estado){
		return endRep.findEnderecoByEstado(estado);
	}
	
	@Override
	public Optional<Endereco> save(Endereco entity) {
		return Optional.of(endRep.save(entity));		
	}

	@Override
	public Optional<Endereco> findById(long id) {
		return endRep.findById(id);
	}

	@Override
	public void delete(Endereco entity) {
		endRep.delete(entity);
	}

	@Override
	public void deleteById(long id) {
		endRep.deleteById(id);
	}

	@Override
	public long count() {
		return endRep.count();
	}

}
