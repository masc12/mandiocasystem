package com.academico.commd.service;

import com.academico.commd.model.Desempenho;
import com.academico.commd.repository.DesempenhoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DesempenhoService implements  CrudInterface<Desempenho> {

    @Autowired
    private DesempenhoRepository desemRep;

    @Override
    public List<Desempenho> findAll() {
        return desemRep.findAll();
    }

    @Override
    public Optional<Desempenho> save(Desempenho entity) {
        return Optional.of(desemRep.save(entity));
    }

    @Override
    public Optional<Desempenho> findById(long id) {
        return desemRep.findById(id);
    }

    @Override
    public void delete(Desempenho entity) {
        desemRep.delete(entity);
    }

    @Override
    public void deleteById(long id) {
        desemRep.deleteById(id);
    }

    @Override
    public long count() {
        return desemRep.count();
    }
}
