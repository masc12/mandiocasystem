package com.academico.commd.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.academico.commd.model.Professor;
import com.academico.commd.repository.ProfessorRepository;

@Service
public class ProfessorService implements CrudInterface<Professor> {

	@Autowired
	private ProfessorRepository professorRep;
	
	public List<Professor> findByLogin(String login){
		return professorRep.findProfessorByLogin(login);
	}
		
	public List<Professor> findByNome(String nome){
		return professorRep.findProfessorByNome(nome);
	}
	
	public List<Professor> findByAreaAtuacao(String areaAtuacao){
		return professorRep.findProfessorByAreaAtuacao(areaAtuacao);
	}
	
	public List<Professor> findByTitulacao(String titulacao){
		return professorRep.findProfessorByTitulacao(titulacao);
	}
	
	@Override
	public List<Professor> findAll() {
		return professorRep.findAll();
	}

	@Override
	public Optional<Professor> save(Professor entity) {
		return Optional.of(professorRep.save(entity));
	}

	@Override
	public Optional<Professor> findById(long id) {
		return professorRep.findById(id);
	}

	@Override
	public void delete(Professor entity) {
		professorRep.delete(entity);
	}

	@Override
	public void deleteById(long id) {
		professorRep.deleteById(id);
		
	}

	@Override
	public long count() {
		return professorRep.count();
	}

}
