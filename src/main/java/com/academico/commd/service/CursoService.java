package com.academico.commd.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.academico.commd.model.Curso;
import com.academico.commd.repository.CursoRepository;

@Service
public class CursoService implements CrudInterface<Curso> {
	
	
	@Autowired
	private CursoRepository cursoRepository;
	
	
	public List<Curso> findByNome(String nome){
		return cursoRepository.findCursoByNome(nome);
	}
	
	public List<Curso> findByTipo(String tipo){
		return cursoRepository.findCursoByTipo(tipo);
	}
	
	
	
	@Override
	public List<Curso> findAll() {
		return cursoRepository.findAll();
	}

	@Override
	public Optional<Curso> save(Curso entity) {
		return Optional.of(cursoRepository.save(entity));
	}

	@Override
	public Optional<Curso> findById(long id) {
		return cursoRepository.findById(id);
	}

	@Override
	public void delete(Curso entity) {
		cursoRepository.delete(entity);
		
	}

	@Override
	public void deleteById(long id) {
		cursoRepository.deleteById(id);
		
	}

	@Override
	public long count() {
		return cursoRepository.count();
	}

}
