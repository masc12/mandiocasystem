package com.academico.commd.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import com.academico.commd.model.Turma;
import com.academico.commd.repository.TurmasRepository;

public class TurmaService implements CrudInterface<Turma> {

	@Autowired
	private TurmasRepository turmaRep;
	
	public List<Turma> findBySemestre(double semestre){
		return turmaRep.findTurmaBySemestre(semestre);
	}
	
	public List<Turma> findByAno(Date ano){
		return turmaRep.findTurmaByAno(ano);
	}
	
	@Override
	public List<Turma> findAll() {
		return turmaRep.findAll();
	}

	@Override
	public Optional<Turma> save(Turma entity) {
		return Optional.of(turmaRep.save(entity));
	}

	@Override
	public Optional<Turma> findById(long id) {
		return turmaRep.findById(id);
	}

	@Override
	public void delete(Turma entity) {
		turmaRep.delete(entity);
		
	}

	@Override
	public void deleteById(long id) {
		turmaRep.deleteById(id);
	}

	@Override
	public long count() {
		return turmaRep.count();
	}

}
