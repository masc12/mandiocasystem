package com.academico.commd.rest;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.academico.commd.model.Professor;
import com.academico.commd.repository.ProfessorRepository;

@RestController
@RequestMapping("/api")
public class ProfessorResource {

	@Autowired
	private ProfessorRepository profRes;

	@GetMapping("/professor")
	public List<Professor> getAllProf(){
		return profRes.findAll();
	}

	@GetMapping("/professor/nome/{nome}")
	public List<Professor> getProfessorByNome(@PathVariable String nome){
		return profRes.findProfessorByNome(nome);
	}

	@GetMapping("/professor/login/{login}")
	public List<Professor> getProfessorByLogin(@PathVariable String login){
		return profRes.findProfessorByLogin(login);
	}

	@GetMapping("/professor/area/{areaAtuacao}")
	public List<Professor> getProfessorByAreaAtuacao(@PathVariable String areaAtuacao){
		return profRes.findProfessorByAreaAtuacao(areaAtuacao);
	}

	@GetMapping("/professor/titulacao/{titulacao}")
	public List<Professor> getProfessorByTitulacao(@PathVariable String titulacao){
		return profRes.findProfessorByTitulacao(titulacao);
	}

	@GetMapping("/professor/id/{id}")
	public ResponseEntity<Professor> findById(@PathVariable Long id){
		Optional<Professor> profById = profRes.findById(id);
        return profById.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

	@PostMapping("/professor")
	public ResponseEntity<Professor> save(@RequestBody @Valid Professor professor){
	    professor = profRes.save(professor);

	    return ResponseEntity.ok().body(professor);
	}

	@PutMapping("/upgrade/professor")
	public ResponseEntity<Professor> upgrade(@RequestBody @Valid Professor professor){
	    professor = profRes.save(professor);

	    return ResponseEntity.ok().body(professor);
	}
}
