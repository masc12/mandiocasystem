package com.academico.commd.rest;

import com.academico.commd.model.Nota;
import com.academico.commd.service.NotaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class NotaResource {

    @Autowired
    private NotaService notaSer;

    @GetMapping("/nota/id/{id}")
    public ResponseEntity<Nota> findByid(@PathVariable Long id){
        Optional<Nota> notabyId = notaSer.findById(id);
        return notabyId.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping("/nota")
    public ResponseEntity<Nota> save(@RequestBody @Valid Nota nota){
        nota = notaSer.save(nota).get();
        return ResponseEntity.ok().body(nota);
    }

    @PutMapping("/nota/upgrade")
    public ResponseEntity<Nota> update(@RequestBody @Valid Nota nota){
        nota = notaSer.save(nota).get();

        return ResponseEntity.ok().body(nota);
    }

}
