package com.academico.commd.rest;

import com.academico.commd.model.Desempenho;
import com.academico.commd.service.DesempenhoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class DesempenhoResource {

    @Autowired
    private DesempenhoService discSer;

    @GetMapping("/desempenho")
    public List<Desempenho> getDesempenhoList(){
        return discSer.findAll();
    }

    @GetMapping("/desempenho/id/{id}")
    public ResponseEntity<Desempenho> findById(@PathVariable Long id){
        Optional<Desempenho> desempenhoById = discSer.findById(id);

        return desempenhoById.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping("/desempenho")
    public ResponseEntity<Desempenho> save(@RequestBody @Valid Desempenho desempenho){
        desempenho = discSer.save(desempenho).get();

        return ResponseEntity.ok().body(desempenho);
    }

    @PutMapping("/desempenho")
    public ResponseEntity<Desempenho> update(@RequestBody @Valid Desempenho desempenho){
        desempenho = discSer.save(desempenho).get();

        return  ResponseEntity.ok().body(desempenho);
    }
}
