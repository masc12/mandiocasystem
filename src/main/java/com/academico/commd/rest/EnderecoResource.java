package com.academico.commd.rest;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import com.academico.commd.service.EnderecoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.academico.commd.model.Endereco;

@RestController
@RequestMapping("/api")
public class EnderecoResource {

	@Autowired
	private EnderecoService endSer;
	
	@GetMapping("/endereco")
	public List<Endereco> getEnderecoList(){
		return endSer.findAll();
	}
	
	@GetMapping("/endereco/rua/{rua}")
	public List<Endereco> getEnderecoByRua(@PathVariable String rua){
		return endSer.findByRua(rua);
	}
	
	@GetMapping("/endereco/cep/{cep}")
	public List<Endereco> getEnderecoByCep(@PathVariable int cep){
		return endSer.findByCep(cep);
	}
	
	@GetMapping("/endereco/cidade/{cidade}")
	public List<Endereco> getEnderecoByCidade(@PathVariable String cidade){
		return endSer.findByCidade(cidade);
	}
	
	@GetMapping("/endereco/estado/{estado}")
	public List<Endereco> getEnderecoByEstado(@PathVariable String estado){
		return endSer.findByEstado(estado);
	}
	
	@GetMapping("/endereco/id/{id}")
	public ResponseEntity<Endereco> findById(@PathVariable Long id){
		Optional<Endereco> endById = endSer.findById(id);
		return endById.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
	}
	
	@PutMapping("/endereco")
	public ResponseEntity<Endereco> save(@RequestBody @Valid Endereco endereco){
		endereco = endSer.save(endereco).get();
		
		return ResponseEntity.ok().body(endereco);
	}
	
	
}
