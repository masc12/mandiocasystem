package com.academico.commd.rest;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.academico.commd.model.Aluno;
import com.academico.commd.service.AlunoService;

@RestController
@RequestMapping("/api")
public class AlunoResource {
	
	@Autowired
	private AlunoService alunoService;
	
	@GetMapping("/aluno")
	public List<Aluno> getAlunosList(){
		return alunoService.findAll();
	}
	
	@GetMapping("/aluno/nome/{nome}")
	public List<Aluno> getAlunoByNome(@PathVariable String nome){
		return alunoService.findByNome(nome);
	}
	
	@GetMapping("/aluno/login/{login}")
	public List<Aluno> getAlunoByLogin(@PathVariable String login){
		return alunoService.findByLogin(login);
	}
	
	@GetMapping("/aluno/{dataVinculo}")
	public List<Aluno> getAlunoByDataVinculo(@PathVariable Date dataVinculo){
		return alunoService.findByDataVinculo(dataVinculo);
	}
	
	@GetMapping("/aluno/{id}")
	public ResponseEntity<Aluno> findbyId(@PathVariable Long id){
		Optional<Aluno> alunoById = alunoService.findById(id);
		return alunoById.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
	}
	
	@PostMapping("/aluno")
	public ResponseEntity<Aluno> save(
			@RequestBody @Valid Aluno aluno){
		aluno = alunoService.save(aluno).get();
		
		return ResponseEntity.ok().body(aluno);
	}
	
	@PutMapping("/aluno")
	public ResponseEntity<Aluno> update(@RequestBody @Valid Aluno aluno){
		aluno = alunoService.save(aluno).get();
		
		return ResponseEntity.ok().body(aluno);
	}
}
