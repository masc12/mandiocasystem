package com.academico.commd.rest;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.academico.commd.model.Turma;
import com.academico.commd.repository.TurmasRepository;

@RestController
@RequestMapping("/api")
public class TurmaResource {

	@Autowired
	private TurmasRepository turmaRep;
	
	@GetMapping("/turma")
	public List<Turma> getAllTurmas(){
		return turmaRep.findAll();
	}
	
	@GetMapping("/turma/semestre/{semestre}")
	public List<Turma> getTurmaBySemestre(@PathVariable double semestre){
		return turmaRep.findTurmaBySemestre(semestre);
	}
	
	@GetMapping("/turma/ano/{ano}")
	public List<Turma> getTurmaByAno(@PathVariable Date ano){
		return turmaRep.findTurmaByAno(ano);
	}
	
	@GetMapping("/turma/id/{id}")
	public ResponseEntity<Turma> findById(@PathVariable Long id){
		Optional<Turma> tumabyId = turmaRep.findById(id);
		return tumabyId.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
	}
	
	@PostMapping("/turma")
	public ResponseEntity<Turma> save(@RequestBody @Valid Turma turma){
		turma = turmaRep.save(turma);
		
		return ResponseEntity.ok().body(turma);
	}
	
	@PutMapping("/turma")
	public ResponseEntity<Turma> upgrade(@RequestBody @Valid Turma turma){
		turma = turmaRep.save(turma);
		
		return ResponseEntity.ok().body(turma);
	}
	
}
