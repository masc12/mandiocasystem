package com.academico.commd.rest;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.academico.commd.model.Disciplina;
import com.academico.commd.service.DisciplinaService;

@RestController
@RequestMapping("/api")
public class DisciplinaResource {
	
	@Autowired
	private DisciplinaService disciplinaService;
	
	@GetMapping("/disciplina")
	public List<Disciplina> getDisciplinaList(){
		return disciplinaService.findAll();
	}
	
	@GetMapping("/disciplina/area/{area}")
	public List<Disciplina> getDisciplinaByArea(@PathVariable String area){
		return disciplinaService.findByArea(area);
	}
	
	
	@GetMapping("/disciplina/nome/{nome}")
	public List<Disciplina> getDisciplinaByNome(@PathVariable String nome){
		return disciplinaService.findByNome(nome);
	}
	
	@GetMapping("/disciplina/id/{id}")
	public ResponseEntity<Disciplina> findById(@PathVariable Long id){
		Optional<Disciplina> disciplinaById = disciplinaService.findById(id);
		return disciplinaById.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
	}
	
	@PostMapping("/disciplina")
	public ResponseEntity<Disciplina> save(@RequestBody @Valid Disciplina disciplina){
		disciplina = disciplinaService.save(disciplina).get();
		
		return ResponseEntity.ok().body(disciplina);
	}

	@PutMapping("/disciplina/upgrade")
	public ResponseEntity<Disciplina> update(@RequestBody @Valid Disciplina disciplina){
		disciplina = disciplinaService.save(disciplina).get();
		
		return ResponseEntity.ok().body(disciplina);
	}
}
