package com.academico.commd.rest;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.academico.commd.model.Usuario;
import com.academico.commd.service.UsuarioService;

@RestController
@RequestMapping("/api")
public class UserResource {

    @Autowired
    private UsuarioService usuarioService;

    @GetMapping("/usuario")
    public List<Usuario> getUserList(){
        return usuarioService.findAll();
    }

    @GetMapping("/usuario/nome/{nome}")
    public List<Usuario> getUserByNome(@PathVariable String nome){
        return usuarioService.findByNome(nome);
    }

    @GetMapping("/usuario/login/{login}")
    public List<Usuario> getUserByLogin(@PathVariable String login){
        return usuarioService.findByLogin(login);
    }

    @GetMapping("/usuario/id/{id}")
    public ResponseEntity<Usuario> findById(@PathVariable Long id){
        Optional<Usuario> usuarioById = usuarioService.findById(id);

        return usuarioById.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping("/usuario")
    public ResponseEntity<Usuario> save(@RequestBody @Valid Usuario usuario){
        usuario = usuarioService.save(usuario).get();

        return ResponseEntity.ok().body(usuario);
    }

    @PutMapping("/update/usuario")
    public ResponseEntity<Usuario> update(@RequestBody @Valid Usuario usuario){
        usuario = usuarioService.save(usuario).get();

        return ResponseEntity.ok().body(usuario);
    }
}
