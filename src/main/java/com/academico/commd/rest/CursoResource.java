package com.academico.commd.rest;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.academico.commd.model.Curso;
import com.academico.commd.service.CursoService;

@RestController
@RequestMapping("/api")
public class CursoResource {
	
	@Autowired
	private CursoService cursoService;
	
	@GetMapping("/curso")
	public List<Curso> getCursosList(){
		return cursoService.findAll();
	}
	
	@GetMapping("/curso/nome/{nome}")
	public List<Curso> getCursoByNome(@PathVariable String nome){
		return cursoService.findByNome(nome);
	}
	
	@GetMapping("/curso/tipo/{tipo}")
	public List<Curso> getCursoByTipo(@PathVariable String tipo){
		return cursoService.findByTipo(tipo);
	}
	
	@GetMapping("/curso/id/{id}")
	public ResponseEntity<Curso> findById(@PathVariable Long id){
		Optional<Curso> cursoById = cursoService.findById(id);
		return cursoById.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
	}
	
	@PostMapping("/curso")
	public ResponseEntity<Curso> save(@RequestBody @Valid Curso curso){
		curso = cursoService.save(curso).get();
		
		return ResponseEntity.ok().body(curso);
	}
	
	@PutMapping("/usuario")
	public ResponseEntity<Curso> update(@RequestBody @Valid Curso curso){
		curso = cursoService.save(curso).get();
		
		return ResponseEntity.ok().body(curso);
	}
}
